import { configureStore } from '@reduxjs/toolkit'
import ShoppingCartReducer from './components/ShoppingCart/ShoppingCartslice'


const store = configureStore({
    reducer: {
        products: ShoppingCartReducer
    }
})

export default store