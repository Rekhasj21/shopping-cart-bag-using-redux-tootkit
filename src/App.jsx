import React, { useEffect } from 'react'
import { useSelector, useDispatch } from 'react-redux'
import Header from './components/Navbar/Navbar'
import ProductsFilter from './components/ProductsFilter/ProductsFilter'
import DisplayProducts from './components/DisplayProducts/DisplayProducts'
import { filteredProducts, closeCart } from './components/ShoppingCart/ShoppingCartslice'
import DisplayCartItems from './components/Cart/DisplayCartItems'

function App() {
  const { productsData, activeSizes, cartItems, totalPrice, showCartItems } = useSelector(store => store.products)
  const dispatch = useDispatch()

  useEffect(() => {
    dispatch(filteredProducts({ productsData, activeSizes }))
  }, [activeSizes])

  const handleCheckout = () => {
    alert(`Checkout - Subtotal: $ ${totalPrice.toFixed(2)}`)
  }

  return (
    <>
      <Header />
      <div className="d-flex justify-content-start align-items-start">
        <ProductsFilter />
        <div className="w-75">
          <p>{productsData.length} Product(s) found</p>
          <div className="d-flex flex-wrap">
            {productsData.map((product) => (
              <DisplayProducts key={product.id} product={product} imageUrl={`/${product.id}_1.jpg`} />
            ))}
          </div>
        </div>
        <div className={`offcanvas offcanvas-end  text-bg-dark ${showCartItems ? "show" : ""}`}
          data-bs-scroll="true" data-bs-backdrop="false" tabIndex={-1} id="offcanvasRight" aria-labelledby="offcanvasRightLabel">
          <div className="offcanvas-header">
            <button type="button" className="btn-close bg-white" data-bs-dismiss="offcanvas" aria-label="Close" onClick={() => dispatch(closeCart())
            } ></button>
          </div>
          <div className="offcanvas-body text-center">
            <div className='d-flex position-relative justify-content-center align-items-center m-2'>
              <img
                className="bg-dark text-end p-3"
                src="/bag-icon.png"
                alt="cart icon"
                width={60}
                height={60}
                data-bs-toggle="offcanvas" data-bs-target="#offcanvasRight" aria-controls="offcanvasRight"
              />
              <button className='btn btn-warning position-absolute top-50 rounded-circle text-dark' style={{
                fontSize: "8px", width: "1.5rem", height: "1.5rem", padding: "0.5rem",
              }}>{cartItems.length}</button>
              <h1 className='fs-4 '>Cart</h1>
            </div>
            <div className='d-flex flex-column bg-dark text-white'>
              {cartItems.length > 0 ? (cartItems.map(cartItem => (
                <DisplayCartItems key={cartItem.id} {...cartItem} />))) : (
                <>
                  <p className='fs-6'>Add some products in the cart</p>
                  <span>:)</span>
                </>
              )}
            </div>
          </div>
          <div className="offcanvas-footer">
            <div className="offcanvas-footer card shadow p-3 mb-5 bg-body-tertiary rounded bg-black text-white">
              <div className='d-flex justify-content-between'>
                <h5 className="card-title text-white text-opacity-50 fs-6">SUBTOTAL</h5>
                <div className="d-flex flex-column justify-content-end align-items-end">
                  <span className='text-warning fs-4'>$ {totalPrice.toFixed(2)}</span>
                  <span className='text-white fs-6 text-opacity-50'>OR UP TO 9 x $ </span>
                </div>
              </div>
              <button className='btn btn-dark text-white' onClick={handleCheckout}>CHECKOUT</button>
            </div>
          </div>
        </div>
      </div>
    </>
  )
}

export default App