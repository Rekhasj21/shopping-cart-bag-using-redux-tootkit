import React from 'react'
import { useSelector, useDispatch } from 'react-redux'
import { selectSizes } from '../ShoppingCart/ShoppingCartslice'
import products from '../../../shopping-cart/data.json'

function ProductsFilter() {
  const { activeSizes } = useSelector(store => store.products)
  const dispatch = useDispatch()
  const sizes = [...new Set(products.products.flatMap(p => p.availableSizes))]

  return (
    <div style={{ width: '300px', marginLeft: "150px" }}>
      <h1 className='fs-4'>Sizes:</h1>
      {sizes.map((size) => (
        <button
          className={`btn btn-secondary opacity-75 text-black rounded-circle m-2 text-center ${activeSizes.includes(size) ? "bg-dark text-white" : ""}`}
          key={size} onClick={() => dispatch(selectSizes(size))} style={{ fontSize: "10px", width: "3rem", height: "3rem", padding: "0.5rem" }}>{size}</button>
      ))}
      <p>Leave a star on Github if this repository was useful :)</p>
      <a href='' className='fs-5'>Star</a>
    </div>
  )
}

export default ProductsFilter