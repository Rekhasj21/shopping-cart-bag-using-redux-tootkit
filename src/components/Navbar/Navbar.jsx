import React from 'react'
import { useSelector } from 'react-redux'

function Navbar() {
    const { cartItems } = useSelector(store => store.products)
    return (
        <nav className="navbar d-flex justify-content-between align-items-end">
            <img className="bg-dark text-end" src="/githubicon.png"
                alt="github icon" width={70} height={70} />
            <div className='d-flex flex-column position-relative'>
                <a className='btn'
                    data-bs-toggle="offcanvas"
                    href="#offcanvasRight" role="button" aria-controls="offcanvasRight">
                    <img
                        className="bg-dark text-end p-3"
                        src="/bag-icon.png" alt="cart icon"
                        width={50} height={50} />
                    <button className='btn btn-warning rounded-circle text-dark position-absolute top-50 start-50 translate-end' style={{
                        fontSize: "8px", width: "1.5rem", height: "1.5rem", padding: "0.5rem",
                    }}> {cartItems.length}</button>
                </a>
            </div>
        </nav>
    )
}

export default Navbar