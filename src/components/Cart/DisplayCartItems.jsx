import React from 'react'
import { useDispatch } from 'react-redux'
import { removeFromCart, addToCart, removeItemFromCart } from '../ShoppingCart/ShoppingCartslice'

function DisplayCartItems(cartItem) {
    const dispatch = useDispatch()
    const { name, price, quantity, imageUrl, style, size, currencyFormat } = cartItem

    const onIncrement = (cartItem) => {
        dispatch(addToCart({
            id: cartItem.id,
            name: cartItem.title,
            price: cartItem.price,
            quantity: { quantity },
            imageUrl: `/${cartItem.id}_2.jpg`,
        }));
    }

    const onDecrement = (cartItem) => {
        dispatch(removeFromCart({
            id: cartItem.id,
            price: cartItem.price,
        }));
    }
    
    return (
        <div className="d-flex justify-content-center shadow-sm p-3 mb-5 bg-body-tertiary rounded" style={{ width: "20rem" }} >
            <img src={imageUrl} className="card-img-top" style={{ width: "4rem" }} alt="..." />
            <div className="card-body d-flex justify-content-between text-white">
                <div className='d-flex flex-column align-items-start m-1'>
                    <h5 className="card-title fs-6 ">{name}</h5>
                    <span className='text-white text-opacity-50'>{size} | {style.split(' ')[0]}</span>
                    <p className='text-white text-opacity-50'>Quantity:{quantity}</p>
                </div>
                <div className='d-flex flex-column align-items-center text-light'>
                    <button type="button" className="btn text-light opacity-50" data-bs-dismiss="modal" aria-label="Close" onClick={() => dispatch(removeItemFromCart(cartItem.id))}>x</button>
                    <span className='text-warning'>{currencyFormat}{price}</span>
                    <div className='d-flex'>
                        <button className='btn btn-dark btn-sm me-1' onClick={() => onDecrement(cartItem)}>-</button>
                        <button className='btn btn-dark btn-sm me-1' onClick={() => onIncrement(cartItem)}>+</button>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default DisplayCartItems