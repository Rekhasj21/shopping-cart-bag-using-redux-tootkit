import { createSlice } from "@reduxjs/toolkit";
import products from '../../../shopping-cart/data.json'

const initialState = {
    productsData: products.products,
    totalPrice: 0,
    activeSizes: [],
    cartItems: [],
    showCartItems: false,
    quantity: 1
}

const ShoppingCartSlice = createSlice({
    name: 'products',
    initialState,
    reducers: {
        selectSizes: (state, { payload }) => {
            let newSizes
            if (state.activeSizes.includes(payload)) {
                newSizes = state.activeSizes.filter((s) => s !== payload)
            } else {
                newSizes = [...state.activeSizes, payload]
            }
            state.activeSizes = newSizes
        },
        filteredProducts: (state, { payload }) => {
            state.productsData = products.products.filter(product => {
                if (payload.activeSizes && payload.activeSizes.length !== 0) {
                    return payload.activeSizes.some(activeSize => {
                        return product.availableSizes.includes(activeSize);
                    });
                }
                return payload.productsData
            });
        },
        addToCart: (state, { payload }) => {
            const existingItem = state.cartItems.find(item => item.id === payload.id);
            if (existingItem) {
                return {
                    ...state, showCartItems: true,
                    cartItems: state.cartItems.map(item => {
                        if (item.id === payload.id) {
                            return {
                                ...item, quantity: item.quantity + 1,
                            };
                        }
                        return item;
                    }),
                    totalPrice: state.totalPrice + payload.price,
                };
            }
            return {
                ...state,
                showCartItems: true,
                cartItems: [
                    ...state.cartItems,
                    {
                        ...payload, quantity: 1,
                    },
                ],
                totalPrice: state.totalPrice + payload.price,
            };
        },
        removeFromCart: (state, { payload }) => {
            const itemToRemove = state.cartItems.find(item => item.id === payload.id);
            if (itemToRemove.quantity === 1) {
                return {
                    ...state,
                    cartItems: state.cartItems.filter(item => item.id !== payload.id),
                    totalPrice: state.totalPrice - itemToRemove.price,
                };
            }
            return {
                ...state,
                cartItems: state.cartItems.map(item => {
                    if (item.id === payload.id) {
                        return {
                            ...item, quantity: item.quantity - 1,
                        };
                    }
                    return item;
                }),
                totalPrice: state.totalPrice - itemToRemove.price,
            };
        },
        removeItemFromCart: (state, { payload }) => {
            const removeItem = state.cartItems.find(item => item.id === payload);
            return {
                ...state,
                cartItems: state.cartItems.filter(item => item.id !== payload),
                totalPrice: state.totalPrice - (removeItem.quantity * removeItem.price),
            }
        },
        closeCart: (state) => {
            state.showCartItems = false
        }
    }
})
export const { selectSizes, filteredProducts, addToCart, removeFromCart, removeItemFromCart, closeCart } = ShoppingCartSlice.actions
export default ShoppingCartSlice.reducer